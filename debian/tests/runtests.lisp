(require "asdf")

;; CL-IRONCLAD is broken on i386 with SBCL.
;; If it were arch:any, we would remove it on that arch.
(when (and (member :sbcl *features*)
	   (member (or (uiop:getenv "DEB_HOST_ARCH")
		       (uiop:stripln
			(uiop:run-program '("dpkg" "--print-architecture")
					  :output :string)))
		   '("i386") :test #'string=))
  (uiop:quit 0))

;; GC debug flags.
#+sbcl
(setf (extern-alien "pre_verify_gen_0" int)  1
      (extern-alien "verify_gens" char)      0
      (extern-alien "gencgc_verbose" int)    1)
#+sbcl (gc)

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "ironclad/tests")
  (asdf:load-system "ironclad-text"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(unless (rtest:do-tests)
    (uiop:quit 1))
